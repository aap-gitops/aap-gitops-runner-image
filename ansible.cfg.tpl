[galaxy]
server_list = automation_hub,galaxy

[galaxy_server.automation_hub]
url=https://cloud.redhat.com/api/automation-hub/ 
auth_url=https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token

token=<AH Token here>

[galaxy_server.galaxy]
url=https://galaxy.ansible.com/
