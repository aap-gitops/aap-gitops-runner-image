# cicd-ansible image using ansible-builder

ansible-builder is a tool for creating Execution Environments for the latest versions of AWX or Red Hat AAP.

## How to build

To build the image either use the [context/Containerfile](context/Containerfile) or rebuild the context and bake an image with:

```bash
ansible-builder build --tag image_name
```

